#!/bin/bash

set -e

source ./setup-tools.sh

if hash deactivate >> /dev/null 2>&1; then
  echo -e "\n***** Python virtualenv loaded, deactivating"
  deactivate
else
  echo -e "\n***** No python virtualenv loaded"
fi

install_base_env

install_apt_package docker.io
install_apt_command golint
install_apt_command meld
install_apt_package nfs-common
install_command pre-commit sudo snap install pre-commit --classic

install_pipx_command sam aws-sam-cli
install_pipx_command aws awscli

install_gem pygments.rb
install_gem redcarpet

install_npm_command instant-markdown-d

if [[ ! -e ~/.aws/config ]]; then
  echo_ "No awscli config, setting default region to eu-west-2."
  export NO_AWS_CONFIG=1
  mkdir -p ~/.aws
  cat > ~/.aws/config <<EOL
[default]
region = eu-west-2
EOL
else
  echo_ "awscli config already exsits"
fi

if grep ^docker /etc/group | grep "$USER" >> /dev/null 2>&1; then
  echo_ "User $USER alread in docker group"
else
  echo_ "User $USER not in docker group, adding"
  sudo usermod -a -G docker "$USER"
fi

if [[ $(git config --global --get diff.tool) != "meld" ]]; then
  echo_ "Git global difftool not 'meld', setting"
  git config --global diff.tool "meld"
else
  echo_ "Git global difftool already 'meld'"
fi

if [[ $(git config --global --get difftool.prompt) != "false" ]]; then
  echo_ "Git global difftool.prompt not 'false', setting"
  git config --global difftool.prompt "false"
else
  echo_ "Git global difftool.prompt already 'false'"
fi

function add_nfs_mount {
  # $1 Directory to mount on nfs server
  # $2 Directory to mount into on client
  # $3 ro/rw (if not set, defaults to rw)
  if [[ "$3" == "ro" ]]; then
    nfs_mode="ro"
  else
    nfs_mode="rw"
  fi

  if grep "^$1" /etc/fstab >>/dev/null 2>&1; then
    echo_ "$1 already in /etc/fstab"
  else
    echo_ "Adding $1 to /etc/fstab mounted onto $2 in $nfs_mode mode"
    echo "$1 $2 nfs $nfs_mode,noauto,nofail,noatime,tcp  0 0" | sudo tee -a /etc/fstab
  fi
}

create_directory /home/eviljonny/nfs/Jonny_Plex_Upload/
create_directory /home/eviljonny/nfs/Max_Plex_Upload/
create_directory /home/eviljonny/nfs/NonPlexMedia/
create_directory /home/eviljonny/nfs/PlexLibrary/
create_directory /home/eviljonny/nfs/Unorganised/

add_nfs_mount smashbook:/mnt/MEDIA_VOL_1/Incoming/Jonny_Plex_Upload/ /home/eviljonny/nfs/Jonny_Plex_Upload/ rw
add_nfs_mount smashbook:/mnt/MEDIA_VOL_1/Incoming/Max_Plex_Upload/ /home/eviljonny/nfs/Max_Plex_Upload/ rw
add_nfs_mount smashbook:/mnt/MEDIA_VOL_1/NonPlexMedia/ /home/eviljonny/nfs/NonPlexMedia/ rw
add_nfs_mount smashbook:/mnt/MEDIA_VOL_1/PlexLibrary/ /home/eviljonny/nfs/PlexLibrary/ ro
add_nfs_mount smashbook:/mnt/MEDIA_VOL_1/Unorganised/ /home/eviljonny/nfs/Unorganised/ rw
