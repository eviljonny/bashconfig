#!/bin/bash

set -e

source ./setup-tools.sh

SSMTP_CONFIGURATION_REQUIRED=0
if sudo test -f /etc/ssmtp/ssmtp.conf; then
  echo_ "ssmtp already configured"
else
  echo_ "ssmtp configuration required"
  SSMTP_CONFIGURATION_REQUIRED=1
  echo "Please enter the gmail app password: "
  IFS= read -rs MAIL_PASSWORD
fi

install_base_env

clone_repo git@github.com:jfharden/rip-scripts.git ~/gitdevel/github.com/jfharden/rip-scripts
symlink ~/gitdevel/github.com/jfharden/rip-scripts/handbrake-scripts ~/handbrake-scripts

if [[ -f /etc/apt/sources.list.d/plexmediaserver.list ]]; then
  echo_ "Plex apt repo aleady added"
else
  echo_ "Adding plex apt repo"
  echo deb https://downloads.plex.tv/repo/deb public main | sudo tee /etc/apt/sources.list.d/plexmediaserver.list
fi

if [[ -f /etc/apt/trusted.gpg.d/plex-sign.gpg ]]; then
  echo_ "Plex apt-key already added"
else
  echo_ "Adding plex gpg apt-key"
  curl https://downloads.plex.tv/plex-keys/PlexSign.key | sudo apt-key --keyring /etc/apt/trusted.gpg.d/plex-sign.gpg add -
fi

install_apt_package vino
install_apt_command sshd
install_apt_package gddrescue
install_apt_package lm-sensors
install_apt_package hddtemp

set_gnome_setting org.gnome.Vino network-interface "'lo'"
set_gnome_setting org.gnome.Vino require-encryption "false"

install_apt_package beignet-opencl-icd


if dpkg -l plexmediaserver >> /dev/null 2>&1 ; then
  echo_ "plexmediaserver already installed"
else
  echo_ "Installing plexmediaserver"
  sudo apt-get install -o Dpkg::Options::="--force-confold" -y plexmediaserver
fi

# echo_ "Updating Plex preferences"
# BACKUP_TIME=$(date +%Y-%m-%d_%H-%M-%S)
# TMP_FILE=$(mktemp /tmp/plex-server-install-XXXXX)
#
# PLEX_PREFS_FILE=/var/lib/plexmediaserver/Library/Application\ Support/Plex\ Media\ Server/Preferences.xml
# sudo cp "$PLEX_PREFS_FILE" "$PLEX_PREFS_FILE.$BACKUP_TIME.bak"
# xq '.Preferences."@Wibble" = "woo"' < "$PLEX_PREFS_FILE" > "$TMP_FILE"

function add_external_disk {
  create_directory "/mnt/$1"

  if findmnt "/mnt/$1" >> /dev/null 2>&1; then
    echo_ "Disk with label $1 is already mounted to /mnt/$1"
  else
    if grep "^LABEL=$1" /etc/fstab >> /dev/null 2>&1; then
      echo_ "Entry for LABEL=$1 is already in fstab"
    else
      echo_ "Creating fstab entry for /mnt/$1"
      sudo cp /etc/fstab "/etc/fstab.$(date +%Y-%m-%d_%H-%M-%S).bak"
      echo "LABEL=$1 /mnt/$1 $2 defaults,auto,rw,nofail,noexec,nosuid,nodev 0 1" | sudo tee -a /etc/fstab
    fi

    echo_ "Mounting /mnt/$1"
    sudo mount "/mnt/$1"
  fi
}

add_external_disk MEDIA_VOL_1 ext4
add_external_disk BACKUP_VOL_1 ext4
add_external_disk BACKUP_VOL_2 ext4

if groups eviljonny | grep plex >> /dev/null; then
  echo_ "eviljonny is already in group plex"
else
  echo_ "Adding eviljonny to plex group"
  sudo usermod -a -G plex eviljonny
fi

function fix_user {
  # Find things which don't have the requested user
  # $1 = directory to start in
  # $2 = user required
  if [[ $(find "$1" -name '*lost+found*' -prune -not -wholename '*lost+found*' -o -not -wholename '*lost+found*' -not -user "$2" | wc -l) -ne 0 ]]; then
    echo_ "Updating files/directories in $1 to have user $2"
    find "$1" \
      -name '*lost+found*' -prune -not -wholename '*lost+found*' \
      -o \
      -not -wholename '*lost+found*' \
      -not -user "$2" \
      -exec sudo chown "$2" {} \;
  else
    echo_ "All files in $1 have user $2"
  fi
}

function fix_group {
  # Find things which don't have the requested group
  # $1 = directory to start in
  # $2 = group required
  if [[ $(find "$1" -name '*lost+found*' -prune -not -wholename '*lost+found*' -o -not -wholename '*lost+found*' -not -group "$2" | wc -l) -ne 0 ]]; then
    echo_ "Updating files/directories in $1 to have the group $2"
    find "$1" \
      -name '*lost+found*' -prune -not -wholename '*lost+found*' \
      -o \
      -not -wholename '*lost+found*' \
      -not -group "$2" \
      -exec sudo chgrp "$2" {} \;
  else
    echo_ "All files in $1 have group $2"
  fi
}

function fix_permissions {
  # find directories which don't permissions 775
  if [[ $(find "$1" -name '*lost+found*' -prune -not -wholename '*lost+found*' -o -not -wholename '*lost+found*' -type d -not -perm 0775 | wc -l) -ne 0 ]]; then
    echo_ "Updating directories in $1 to have permissions 0775"
    find "$1" \
      -name '*lost+found*' -prune -not -wholename '*lost+found*' \
      -o \
      -not -wholename '*lost+found*' \
      -type d -not -perm 0775 \
      -exec sudo chmod 0775 {} \;
  else
    echo_ "All directories in $1 have permissions 0775"
  fi

  # find files which don't have permissions 664
  if [[ $(find "$1" -name '*lost+found*' -prune -not -wholename '*lost+found*' -o -not -wholename '*lost+found*' -type f -not -perm 0664 | wc -l) -ne 0 ]]; then
    echo_ "Updating files in $1 to have permissions 0664"
    find "$1" \
      -name '*lost+found*' -prune -not -wholename '*lost+found*' \
      -o \
      -not -wholename '*lost+found*' \
      -type f -not -perm 0664 \
      -exec sudo chmod 0664 {} \;
  else
    echo_ "All files in $1 have permissions 0664"
  fi
}

function fix_ownership_and_permissions {
  # $1 = directory
  # $2 = user
  # $3 = group
  fix_user "$1" "$2"
  fix_group "$1" "$3"
  fix_permissions "$1"
}

create_directory /mnt/MEDIA_VOL_1/PlexLibrary/

symlink /mnt/MEDIA_VOL_1/PlexLibrary ~/plexlib
symlink /mnt/MEDIA_VOL_1 ~/media

function add_nfs_export {
  # $1 Directory to export
  # $2 anonuid
  # $3 anongid
  # $4 ro/rw (if not set, defaults to rw)
  #
  # Sets NFS_RELOAD_REQUIRED to 1 if it modifies the exports config

  if [[ "$4" == "ro" ]]; then
    nfs_mode="ro"
  else
    nfs_mode="rw"
  fi

  if grep "^$1" /etc/exports >>/dev/null 2>&1; then
    echo_ "$1 already in /etc/exports"
  else
    echo_ "Adding $1 to /etc/exports with anonuid=$2,anongid=$3"
    NFS_RELOAD_REQUIRED=1
    echo "$1 192.168.178.0/24($nfs_mode,sync,no_subtree_check,all_squash,anonuid=$2,anongid=$3,insecure)" | sudo tee -a /etc/exports
  fi
}

function setup_nfs_server {
  create_directory /mnt/MEDIA_VOL_1/Incoming
  create_directory /mnt/MEDIA_VOL_1/Incoming/Max_Plex_Upload
  create_directory /mnt/MEDIA_VOL_1/Incoming/Jonny_Plex_Upload

  install_apt_package nfs-kernel-server

  fix_ownership_and_permissions /mnt/MEDIA_VOL_1/Incoming/ nobody plex

  nobody_uid=$(id -u nobody)
  plex_gid=$(id -g plex)

  NFS_RELOAD_REQUIRED=0

  add_nfs_export /mnt/MEDIA_VOL_1/Incoming/Max_Plex_Upload "$nobody_uid" "$plex_gid" "rw"
  add_nfs_export /mnt/MEDIA_VOL_1/Incoming/Jonny_Plex_Upload "$nobody_uid" "$plex_gid" "rw"
  add_nfs_export /mnt/MEDIA_VOL_1/Unorganised/ "$nobody_uid" "$plex_gid" "rw"
  add_nfs_export /mnt/MEDIA_VOL_1/NonPlexMedia/ "$nobody_uid" "$plex_gid" "rw"
  add_nfs_export /mnt/MEDIA_VOL_1/PlexLibrary/ "$nobody_uid" "$plex_gid" "ro"
  add_nfs_export /mnt/MEDIA_VOL_1/RetroGames/ "$nobody_uid" "$plex_gid" "ro"

  # We need rpc-statd so OSX (specifically Big Sur) can mount the nfs share)
  if systemctl is-enabled rpc-statd >> /dev/null 2>&1; then
    echo_ "rpc-statd is already enabled"
  else
    echo_ "Enabling system service rpc-statd"
    NFS_RELOAD_REQUIRED=1
    sudo systemctl enable rpc-statd
  fi

  if systemctl is-active rpc-statd >> /dev/null 2>&1; then
    echo_ "rpc-statd already running"
  else
    echo_ "Starting rpc-statd"
    NFS_RELOAD_REQUIRED=1
    sudo systemctl start rpc-statd
  fi

  if [[ $NFS_RELOAD_REQUIRED -eq 1 ]]; then
    echo_ "Reloading NFS server"
    sudo systemctl reload nfs-kernel-server
  else
    echo_ "NFS server reload not required"
  fi
}

setup_nfs_server

fix_ownership_and_permissions /mnt/MEDIA_VOL_1/PlexLibrary/ eviljonny plex

if grep "^deb http://ppa.launchpad.net/stebbins/handbrake-releases/ubuntu" >/dev/null 2>&1 \
  /etc/apt/sources.list \
  /etc/apt/sources.list.d/*.list
then
  echo_ "Handbrake PPA already in sources"
else
  echo_ "Adding Handbrake PPA to sources"
  sudo add-apt-repository -y ppa:stebbins/handbrake-releases
fi

install_apt_package mkvtoolnix
install_apt_package ffmpeg
install_apt_package handbrake-gtk
install_apt_package handbrake-cli
install_apt_package id3v2

install_apt_package libdvd-pkg

echo_ "Making sure libdvd-pkg is built"
sudo dpkg-reconfigure libdvd-pkg

install_snap_package makemkv
add_snap_permission makemkv optical-write optical-drive

if grep -r "^HandleLidSwitch=ignore" /etc/systemd/logind.conf* >/dev/null 2>&1
then
  echo_ "Systemd Login Confg already set to ignore Lid Switch"
else
  echo_ "Adding HandleLidSwitch=ignore to systemd login conf"
  create_directory /etc/systemd/logind.conf.d
  sudo chmod 755 /etc/systemd/logind.conf.d
  echo "[Login]
HandleLidSwitch=ignore
HandleLidSwitchDocked=ignore
HandleLidSwitchExternalPower=ignore" | sudo tee /etc/systemd/logind.conf.d/ignore-lid-switch.conf
  sudo chmod 644 /etc/systemd/logind.conf.d/ignore-lid-switch.conf
  sudo chown root:root /etc/systemd/logind.conf.d/ignore-lid-switch.conf
  echo
  echo "SYSTEM MUST BE RESTARTED TO FIX LAPTOP LID SWITCH BEHAVIOUR"
  echo

  if [[ ! -f /var/run/reboot-required ]]; then
    echo "*** System restart required ***" | sudo tee /var/run/reboot-required
  fi
fi

# These 6 packages are for cdripping using abcde, annoyingly abcde want's postfix installed too
install_apt_package lame
install_apt_package eyed3
install_apt_package glyrc
install_apt_package imagemagick
install_apt_package flac
install_apt_package brasero
install_apt_package_without_recs abcde

if [[ ! -f "$HOME/.abcde.conf" ]]; then
  echo_ "Creating abcde conf in $HOME/.abcde.conf"
  cat > "$HOME/.abcde.conf" << 'EOF'
# See /etc/abcde.conf for all options
PADTRACKS=y
FLACOPTS='-s -e -V -8'
OUTPUTTYPE='flac'
OUTPUTDIR='/mnt/MEDIA_VOL_1/Unorganised'
OUTPUTFORMAT='${ARTISTFILE}/${ALBUMFILE}/${TRACKNUM} - ${TRACKFILE}'
VAOUTPUTFORMAT='Various Artists/${ALBUMFILE}/${TRACKNUM} - ${TRACKFILE}'
MAXPROCS=4
CDDBMETHOD=musicbrainz
ACTIONS=default,getalbumart
GLYRC=glyrc
GLYRCOPTS=
IDENTIFY=identify
IDENTIFYOPTS=
ALBUMARTALWAYSCONVERT="n"
ALBUMARTFILE="cover.jpg"
ALBUMARTTYPE="JPEG"
EOF
else
  echo_ "$HOME/.abcde.conf already exists"
fi

function link_plex_server_script {
  if [ ! -f "$HOME/bin/$1" ]; then
    echo_ "Creating Link in $HOME/bin/ to $1 script"
    ln -s "$(realpath "./plex-server-scripts/$1")" "$HOME/bin/$1"
  else
    echo_ "$HOME/bin/$1 already exists"
  fi
}

link_plex_server_script backup-plex-and-media
link_plex_server_script bandcamp2plex
link_plex_server_script reverse-series-extras
link_plex_server_script scan
link_plex_server_script convert-m4v-to-mkv
link_plex_server_script fix-multi-disc-track-numbers
link_plex_server_script fix-various-artist-labels
link_plex_server_script set-disc-number

link_bash_customisation 50 ejects.bashrc
link_bash_customisation 55 plex-nav.bashrc

if ! grep 'Unattended-Upgrade::Mail ""' /etc/apt/apt.conf.d/50unattended-upgrades >> /dev/null; then
  echo_ "Unattended Upgrades mailer address already configured"
else
  echo_ "Configuring unattended upgrades to mail status"
  sudo sed -i -E \
    's/^.*Unattended-Upgrade::Mail "".*$/Unattended-Upgrade::Mail "jfharden+plex-server-updates@gmail.com";/' \
    /etc/apt/apt.conf.d/50unattended-upgrades
fi

if grep '^Unattended-Upgrade::MailReport "on-change"' /etc/apt/apt.conf.d/50unattended-upgrades >> /dev/null; then
  echo_ "Unattended Upgrades mail report status already set to on-change"
else
  echo_ "Configuring unattended upgrades to mail reports on-change"
  sudo sed -i -E \
    's/^.*Unattended-Upgrade::MailReport ".*$/Unattended-Upgrade::MailReport "on-change";/' \
    /etc/apt/apt.conf.d/50unattended-upgrades
fi

install_apt_package ssmtp
install_apt_package cowsay

if [ "$SSMTP_CONFIGURATION_REQUIRED" -eq 1 ]; then
  echo_ "Setting up ssmtp"
  sudo cp /etc/ssmtp/ssmtp.conf "/etc/ssmtp/ssmtp.$(date +%Y-%m-%d_%H-%M-%S).bak"
  sudo tee /etc/ssmtp/ssmtp.conf >>/dev/null << EOF
# Config file for sSMTP sendmail
#
# The person who gets all mail for userids < 1000
# Make this empty to disable rewriting.
root=jfharden@gmail.com

# The place where the mail goes. The actual machine name is required no
# MX records are consulted. Commonly mailhosts are named mail.domain.com
mailhub=smtp.gmail.com:587

AuthUser=jfharden@gmail.com
AuthPass=${MAIL_PASSWORD}
AuthMethod=LOGIN
UseTLS=YES
UseSTARTTLS=YES

# Where will the mail seem to come from?
rewriteDomain=gmail.com

# The full hostname
hostname=$(hostname -A)

# Are users allowed to set their own From: address?
# YES - Allow the user to specify their own From: address
# NO - Use the system generated From: address
FromLineOverride=Yes
EOF
fi

if [[ -f ~/.ssh/authorized_keys ]]; then
  # shellcheck disable=SC2088
  echo_ "~/.ssh/authorized_keys already exists you do not need to copy in any public keys"
else
  echo_ "You need to copy in your SSH public key to ~/.ssh/authorized_keys"
fi

check_if_restart_required
