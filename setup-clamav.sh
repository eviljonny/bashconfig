#!/bin/bash

set -e

source ./setup-tools.sh

install_apt_package clamav
install_apt_package clamav-daemon
install_apt_package clamav-freshclam
install_apt_package clamtk
