#!/bin/bash

function echo_ {
  echo -e "\n***** $@"
}

function create_directory {
  if [[ -d "$1" ]]; then
    echo_ "Directory '$1' already exists"
  else
    echo_ "Creating directory '$1'"
    sudo mkdir -p "$1"
  fi
}

function install_apt_package {
  if dpkg -l "$1" | grep '^ii' >> /dev/null 2>&1; then
    echo_ "Apt package $1 already installed"
  else
    echo_ "Need to install apt package $1"
    sudo apt-get install -y "$1"
  fi
}

function install_apt_package_without_recs {
  if dpkg -l "$1" | grep '^ii' >> /dev/null 2>&1; then
    echo_ "Apt package $1 already installed"
  else
    echo_ "Need to install apt package $1 without recommendations"
    sudo apt-get install --no-install-recommends -y "$1"
  fi
}

function install_snap_package {
  if snap list "$1" >>/dev/null 2>&1 ; then
    echo_ "Snap Package $1 already installed"
  else
    echo_ "Need to install snap package $1"
    sudo snap install "$1"
  fi
}

function add_snap_permission {
  # $1 = snap
  # $2 = plug
  # $3 = connection
  # E.G. For `snap connect makemkv:optical-write :optical-drive`
  #  add_snap_permission makemkv optical-write optical_drive
  if snap connections "$1" | grep "$1:$2" | grep ":$3" >>/dev/null 2>&1; then
    echo_ "Snap $1 already has plug $2 connected to $3"
  else
    echo_ "Snap $1 needs plug $2 connecting to $3"
    sudo snap connect "$1:$2" ":$3"
  fi
}

function install_command {
  if hash "$1" >> /dev/null 2>&1; then
    echo_ "$1 already installed"
  else
    echo_ "Need to install $1, installing with command 'eval ${@:2}'"
    eval ${@:2}
  fi
}

function install_apt_command {
  install_command "$1" sudo apt-get install -y "$1"
}

function install_npm_command {
  install_command "$1" npm -g install "$1"
}

function install_pipx_command {
  install_command "$1" pipx install "$2"
}

function install_gem {
  if gem list -i "$1" >> /dev/null 2>&1; then
    echo -e "\n***** $1 ruby gem already installed"
  else
    echo -e "\n***** Installing $1 ruby gem"
    gem install "$1"
  fi
}

function clone_repo {
  if [[ ! -d $2 ]]; then
    echo -e "\n***** Cloning $1"
    git clone "$1" "$2"
  else
    echo -e "\n***** $1 already cloned"
  fi
}

function symlink {
  if [[ -e $2 ]]; then
    if [[ ! -L $2  ]]; then
      echo_ "$2 is not a symlink, backing up to $2.original and replacing with symlink to $1"
      mv "$2" "$2.original"
      ln -s "$1" "$2"
    else
      echo -e "\n***** $2 already a symlink"
    fi
  else
    echo_ "$2 does not exist, creating a symlink pointing to $1"
    ln -s "$1" "$2"
  fi
}

function add_asdf_plugin {
  if asdf plugin list | grep "$1" >> /dev/null 2>&1; then
    echo_ "asdf plugin $1 already installed"
  else
    asdf plugin add "$1"
  fi
}

function add_default_asdf_tool_version {
  if grep "^$1 $2" ~/.tool-versions >> /dev/null 2>&1; then
    echo_ "asdf default version '$1 $2' already set"
  elif grep "^$1 " ~/.tool-versions >> /dev/null 2>&1; then
    echo_ "asdf version for $1 is set to $2, changing"
    sed -i -E "s/^$1 .*$/$1 $2/" ~/.tool-versions
  else
    echo_ "No default asdf version set for $1, setting to $2"
    echo "$1 $2" >> ~/.tool-versions
  fi
}

function install_base_env {
  if [ ! -d "$HOME/bin" ]; then
    echo_ "Creating $HOME/bin directory"
    mkdir "$HOME/bin"
  else
    echo_ "$HOME/bin already exists"
  fi

  echo_ "Updating apt"
  sudo apt-get update

  install_apt_command git
  install_apt_command gcc
  install_apt_command curl
  install_apt_command ctags
  install_apt_command less
  install_apt_command make
  install_apt_command jq
  install_apt_command rename
  install_apt_command screen
  install_apt_command tree
  install_apt_command unrar
  install_apt_command vim
  install_apt_command vlc
  install_apt_package git-gui
  install_apt_package meld
  install_command ag sudo apt-get install -y silversearcher-ag

  mkdir -p ~/gitdevel/bitbucket.org/eviljonny
  mkdir -p ~/gitdevel/github.com/jfharden

  clone_repo git@bitbucket.org:eviljonny/bashconfig.git ~/gitdevel/bitbucket.org/eviljonny/bashconfig
  symlink ~/gitdevel/bitbucket.org/eviljonny/bashconfig/bashrc ~/.bashrc
  
  clone_repo git@bitbucket.org:eviljonny/vimconfig.git ~/gitdevel/bitbucket.org/eviljonny/vimconfig
  symlink ~/gitdevel/bitbucket.org/eviljonny/vimconfig ~/.vim

  if [[ -f ~/.vimrc ]]; then
    echo -e "\n***** ~/.vimrc exists, moving to ~/.vimrc-original"
    mv ~/.vimrc ~/.vimrc-original
  else
    echo -e "\n***** .vimrc doesn't exist, don't need to remove it"
  fi

  install_command asdf git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.7.8

  clone_repo git@github.com:jfharden/work-setup-scripts.git ~/gitdevel/github.com/jfharden/work-setup-scripts

  echo_ "Sourcing bashrc"
  source ~/.bashrc

  echo_ "Installing asdf plugins"
  set +e
  asdf plugin add golang
  asdf plugin add nodejs
  asdf plugin add python
  asdf plugin add ruby
  asdf plugin add shellcheck
  asdf plugin add terraform
  asdf plugin add terraform-docs
  asdf plugin add terragrunt
  asdf plugin add tflint
  asdf plugin add yarn
  set -e

  if [[ -f ~/.asdfrc ]] && grep "legacy_version_file = yes" ~/.asdfrc >>/dev/null 2>&1; then
    # shellcheck disable=SC2088
    echo_ "~/.asdfrc already exists and contains 'legacy_version_file = yes'"
  elif [[ -f ~/.asdfrc ]]; then
    # shellcheck disable=SC2088
    echo_ "~/.asdfrc does not contain 'legacy_version_file = yes', adding"
    echo "legacy_version_file = yes" >> ~/.asdfrc
  else
    # shellcheck disable=SC2088
    echo_ "~/.asdfrc doesn't exist, adding with 'legacy_version_file = yes'"
    echo "legacy_version_file = yes" >> ~/.asdfrc
  fi

  echo_ "Installing python build dependencies for ssl, zip, bzip2, readline, sqlite3, lzma, ctypes"
  sudo apt-get install -y \
    libffi-dev `# ctypes support` \
    libssl-dev `# ssl support` \
    libbz2-dev `# bzip2 support` \
    libreadline-dev `# readline support` \
    libsqlite3-dev `# sqlite3 support` \
    liblzma-dev `# lzma support` \
    zlib1g-dev `# zip support`
  
  if [[ ! -f ~/.tool-versions ]]; then
    echo_ "No default tool versions installed, adding"
    cat > ~/.tool-versions <<EOF
terraform 0.14.7
terraform-docs v0.9.1
yarn 1.22.4
nodejs 14.15.0
python 3.9.1
ruby 2.7.2
shellcheck 0.7.1
EOF
  else
    echo_ "Default asdf tool-versions already exists"
  fi

  echo_ "Installing nodejs releast team keyring"
  ~/.asdf/plugins/nodejs/bin/import-release-team-keyring

  echo_ "Installing default asdf tools"
  asdf install

  if [[ $(git config --global --get core.editor) != "vim" ]]; then
    echo_ "Setting git editor to vim globally"
    git config --global core.editor vim
  else
    echo_ "Git editor already vim globally"
  fi

  if [[ $(git config --global --get user.name) != "Jonathan Harden" ]]; then
    echo_ "Git global username not 'Jonathan Harden', setting"
    git config --global user.name "Jonathan Harden"
  else
    echo_ "Git global username already 'Jonathan Harden'"
  fi

  if [[ $(git config --global --get user.email) != "jfharden@gmail.com" ]]; then
    echo_ "Git global user email not 'jfharden@gmail.com', setting"
    git config --global user.email "jfharden@gmail.com"
  else
    echo_ "Git global user email already 'jfharden@gmail.com'"
  fi

  if [[ $(git config --global --get diff.tool) != "meld" ]]; then
    echo_ "Git global difftool not 'meld', setting"
    git config --global diff.tool meld
  else
    echo_ "Git global difftool already 'meld'"
  fi

  if [[ $(git config --global --get difftool.prompt) != "false" ]]; then
    echo_ "Git global difftool prompt not 'false', setting"
    git config --global difftool.prompt "false"
  else
    echo_ "Git global difftool prompt already 'false'"
  fi

  install_command pipx python -m pip install --user pipx

  # Read the output of this to make sure the executables will run
  if ! python -m pipx ensurepath; then
    echo "Pipx ensurepath failed.........you need to fix the errors above before running setup again"
    exit 1
  fi

  install_pipx_command yq yq

  create_directory "$HOME/.bashrc.d/"
}

function check_if_restart_required {
  if [[ -f /var/run/reboot-required ]]; then
    echo
    cat /var/run/reboot-required
  fi
}

function set_gnome_setting {
  # $1 = Which tree (e.e. org.gnome.Vino)
  # $2 = which setting (e.g. network-interface)
  # $3 = desired value

  if [[ $(gsettings get "$1" "$2") == "$3" ]]; then
    echo_ "Gnome setting $1 $2 is already set to $3"
  else
    echo_ "Setting Gnome setting $1 $2 to $3"
    if ! gsettings set "$1" "$2" "$3"; then
      echo_ "FAILED TO Gnome setting $1 $2 to $3!!!"
    fi
  fi
}

function link_bash_customisation {
  if [ ! -f "$HOME/.bashrc.d/$1-$2" ]; then
    echo_ "Creating Link in $HOME/.bashrc.d/ to $2 script with priority $1"
    ln -s "$(realpath "./bash-customisations/$2")" "$HOME/.bashrc.d/$1-$2"
  else
    echo_ "$HOME/.bashrc.d/$1-$2 already exists"
  fi
}
