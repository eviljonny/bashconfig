#!/bin/bash

set -euo pipefail

source ./setup-tools.sh

SETUP_SCRIPTS_DIR="$HOME/gitdevel/github.com/jfharden/work-setup-scripts"

function work-setup-usage {
  echo "Usage:"
  echo "  $0 <workplace>/<machine-type>"
  echo ""
  echo "Valid options:"
  for MACHINE in $(list-known-machines); do
    echo "    $MACHINE"
  done
  echo ""
  echo "Example:"
  echo "    $0 aimar-foundation/ubuntu"
  echo ""
  exit 1
}

function list-known-machines {
  pushd "$SETUP_SCRIPTS_DIR" >> /dev/null

  find . -mindepth 1 -maxdepth 1 -type d | sed -E 's@^./@@' | sort | while read -r ORG; do
    find "./$ORG" -mindepth 1 -maxdepth 1 -type f -name '*.sh' | sed -E 's@^./@@' | sort | while read -r MACHINE; do
      echo "$MACHINE" | sed -E 's/\.sh$//'
    done
  done

  popd >> /dev/null
}

if [[ $# -ne 1 ]]; then
  work-setup-usage
fi
WORKPLACE=$(echo "$1" | cut -f 1 -d /)
MACHINE_TYPE=$(echo "$1" | cut -f 2 -d /)

WORKPLACE_SETUP_SCRIPT="$SETUP_SCRIPTS_DIR/$WORKPLACE/$MACHINE_TYPE.sh"

if [[ ! -x "$WORKPLACE_SETUP_SCRIPT" ]]; then
  echo "ERROR: $WORKPLACE_SETUP_SCRIPT either does not exist or is not executable"
  echo
  work-setup-usage
fi

echo "Are you sure you wish to execute $WORKPLACE_SETUP_SCRIPT?"
echo "Only 'yes' will cause the script to be executed, any other response will cause premature exit: "
IFS= read -r EXECUTE

if [[ "$EXECUTE" != "yes" ]]; then
  echo_ "Exiting without further changes"
  exit 1
fi

BASH_DIR=$(dirname "${BASH_SOURCE[0]}")
SETUP_TOOLS_FILE=$(realpath "$BASH_DIR/setup-tools.sh")
"$WORKPLACE_SETUP_SCRIPT" "$SETUP_TOOLS_FILE"
