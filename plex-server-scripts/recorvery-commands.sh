#!/bin/bash

echo "This is not the actual script to run. Just commands stored."
echo "Needs to be manually updated for every individual case."
exit 1

if [ ! -f ./rescue.img ]; then
  ddrescue -r3 -d /dev/cdrom ./rescue.img ./rescue.log
  ddrescue -r3 /dev/cdrom ./rescue.img ./rescue.log

  if [ ! -d /mnt/rescue ]; then
    mkdir /mnt/rescue
  fi

  sudo mount -o loop,uid=eviljonny,gid=eviljonny ./rescue.img /mnt/rescue/
fi

