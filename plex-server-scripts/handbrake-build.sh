#!/bin/bash

# See https://handbrake.fr/docs/en/latest/developer/build-linux.html for original and up to date instructions

set -e

sudo apt-get install -y \
    autoconf \
    automake \
    autopoint \
    build-essential \
    cmake \
    git \
    libass-dev \
    libbz2-dev \
    libfontconfig1-dev \
    libfreetype6-dev \
    libfribidi-dev \
    libharfbuzz-dev \
    libjansson-dev \
    liblzma-dev \
    libmp3lame-dev \
    libnuma-dev \
    libogg-dev \
    libopus-dev \
    libsamplerate-dev \
    libspeex-dev \
    libtheora-dev \
    libtool \
    libtool-bin \
    libturbojpeg0-dev \
    libvorbis-dev \
    libx264-dev \
    libxml2-dev \
    libvpx-dev \
    m4 \
    make \
    meson \
    nasm \
    ninja-build \
    patch \
    pkg-config \
    python \
    tar \
    zlib1g-dev \

sudo apt-get install -y \
  libva-dev \
  libdrm-dev

if [ ! -d ./HandBrake ];then
  git clone https://github.com/HandBrake/HandBrake.git
fi

pushd HandBrake || exit 1

./configure --launch-jobs="$(nproc)" --launch --enable-qsv --disable-gtk

sudo make --directory=build install

if [ ! -d "$HOME/bin" ]; then
  mkdir "$HOME/bin"
fi

cp build/HandBrakeCLI "$HOME/bin"

popd
