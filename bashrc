#!/bin/bash

if [[ -n "$ZSH_VERSION" ]]; then
  JFH_SHELL=zsh
  autoload -Uz compinit && compinit -u
  compaudit
  if [[ $? -ne 0 ]]; then
    echo "The above directories have insecure permissions, remove group write for them!"
  fi
  autoload -Uz bashcompinit && bashcompinit
elif [[ -n "$BASH_VERSION" ]]; then
  JFH_SHELL=bash
else
  echo "Unkown shell"
  return 0
fi

export HISTSIZE=100000
export HISTFILESIZE=100000
# Default editor when terminal supports full screen mode (almost always!)
export VISUAL=vim

_find_aws_autocomplete()
{
  _script_commands='in instance instances ip'

  local cur
  COMPREPLY=()
  cur="${COMP_WORDS[COMP_CWORD]}"
  COMPREPLY=( $(compgen -W "${_script_commands}" -- ${cur}) )

  return 0
}
complete -o nospace -F _find_aws_autocomplete findaws

colour_red () { 
  wrap_colour "\033[31m"
}

colour_light_red () {
  wrap_colour "\033[91m"
}

colour_yellow () {
  wrap_colour "\033[33m"
}

colour_light_yellow () {
  wrap_colour "\033[93m"
}

colour_green () {
  wrap_colour "\033[32m"
}

colour_light_green () {
  wrap_colour "\033[92m"
}

colour_cyan () {
  wrap_colour "\033[36m"
}

colour_blue () {
  wrap_colour "\033[34m"
}

colour_light_blue () {
  wrap_colour "\033[94m"
}

colour_magenta() {
  wrap_colour "\033[35m"
}

colour_white() {
  wrap_colour "\033[37m"
}

reset_colour() {
  wrap_colour "\033[00m"
}

wrap_colour() {
  if [[ $JFH_SHELL == "zsh" ]]; then
    echo "!%{$1%}"
  elif [[ $JFH_SHELL == "bash" ]]; then
    echo "\[$1\]"
  fi
}

colour_danger() {
  if [[ "$JONNY_TERM_COLOUR_SCHEME" == "solarized" ]]; then
    colour_red
  else
    colour_light_red
  fi
}

colour_warning() {
  colour_yellow
}

colour_safe() {
  if [[ "$JONNY_TERM_COLOUR_SCHEME" == "solarized" ]]; then
    colour_cyan
  else
    colour_green
  fi
}

colour_other() {
  colour_blue
}

find_aws_instances () {
  aws ec2 describe-instances \
    --region=eu-west-1 \
    --instance-ids \
    --filters "Name=tag:Name,Values=*$1*" \
    --output text \
    --query 'Reservations[].Instances[].[InstanceId, Tags[?Key==`Name`].Value | [0],PrivateIpAddress, State.Name, LaunchTime]' | \
    column -t
}

find_aws_ip () {
  aws ec2 describe-instances \
    --region=eu-west-1 \
    --instance-ids \
    --filters "Name=private-ip-address,Values=*$1*" \
    --output text \
    --query 'Reservations[].Instances[].[InstanceId, Tags[?Key==`Name`].Value | [0],PrivateIpAddress, State.Name, LaunchTime]' | \
    column -t
}

find_aws () {
  if [[ "$1" == "ip" ]]; then
    find_aws_ip $2
  elif [[ "$1" == "instance" ]] || [[ "$1" == "instances" ]] || [[ "$1" == "in" ]]; then
    find_aws_instances $2
  else
    echo "Usage: findaws <ip|instance> [search]"
  fi
}

aws_creds_colour () {
  case $1 in
    jfharden)         echo "$(colour_danger)" ;;
    jfharden-sandbox) echo "$(colour_safe)" ;;
    pomegranate)      echo "$(colour_danger)" ;;
    no_aws)           echo "$(colour_safe)" ;;
    *)                echo "$(colour_other)" ;;
  esac
}

aws_creds () {
  if [ "$JONNY_AWS_IN_ENV" ]; then
    echo "$(aws_creds_colour $JONNY_AWS_IN_ENV)aws_env_$JONNY_AWS_IN_ENV$(reset_colour)"
  elif [ "$AWS_PROFILE" ]; then
    echo "$(aws_creds_colour $AWS_PROFILE)aws_profile_$AWS_PROFILE$(reset_colour)"
  else
    echo "$(aws_creds_colour "no_aws")no_aws$(reset_colour)"
  fi
}

heart_emoji() {
  if [[ $JFH_SHELL == "zsh" ]]; then
    echo "%{♥%}"
  elif [[ $JFH_SHELL == "bash" ]]; then
    echo "\342\231\245"
  fi
}

emoji () {
    if [ $1 -eq 0 ]; then
        # Solid green heart
        echo "$(colour_safe)$(heart_emoji)$(reset_colour)"
    elif [ $1 -gt 0 ]; then 
        # Solid red heart
        echo "$(colour_danger)$(heart_emoji)$(reset_colour)"
    fi
}

find_by_name () {
    find . -name '*'$1'*'
}

git_branch_prompt() {
  BRANCH=$(git_branch)
  echo "($(git_branch_colour ${BRANCH})${BRANCH}$(reset_colour))"
}

short_python_virtualenv() {
  basename $VIRTUAL_ENV
}

python_virtualenv_prompt() {
  if [ "$VIRTUAL_ENV" != "" ]; then
    echo "$(colour_safe)python_venv($(reset_colour)$(short_python_virtualenv)$(colour_safe))$(reset_colour) :"
  fi
}

git_branch() {
  git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/'
}

git_branch_colour() {
  case $1 in
    master|main) echo "$(colour_danger)" ;;
    *)      echo "$(colour_safe)" ;;
  esac
}

zsh_prompt() {
  PS1="$(date +%H:%M) : $(hostname) : $(aws_creds) : $(git_branch_prompt) : $(python_virtualenv_prompt) [%~] "$'\n'"$(emoji $1) \$ "
}

if [[ $JFH_SHELL == "zsh" ]]; then
  precmd() {
    zsh_prompt $?
  }
elif [[ $JFH_SHELL == "bash" ]]; then
  export PROMPT_COMMAND='EXIT_STATUS=$? BRANCH=$(git_branch) PS1="`date +"%H:%M"` : $(hostname) : $(aws_creds) : $(git_branch_prompt) : $(python_virtualenv_prompt)[\w] \n$(emoji $EXIT_STATUS) \$ "'
fi

if [[ $JFH_SHELL == "bash" ]]; then
  SOURCE_FILE_LOCATION=${BASH_SOURCE[0]}
  if [[ -L $SOURCE_FILE_LOCATION ]]; then
    SOURCE_FILE_LOCATION=$( readlink $SOURCE_FILE_LOCATION )
  fi
  DIR="$( cd "$( dirname "$SOURCE_FILE_LOCATION" )" && pwd )"
  source $DIR/git-completion.bash
fi

alias findn=find_by_name
alias findawsinstances=find_aws_instances
alias findawsip=find_aws_ip
alias findaws=find_aws

complete -C aws_completer aws

alias exportawsprofile-pomegranate='export AWS_PROFILE=pomegranate'
alias exportawsprofile-jfharden='export AWS_PROFILE=jfharden'
alias exportawsprofile-sandbox='export AWS_PROFILE=jfharden-sandbox'

alias cdp='cd ~/gitdevel/github.com/pomegranate-technology/'
alias cdj='cd ~/gitdevel/github.com/jfharden/'
alias cde='cd ~/gitdevel/bitbucket.org/eviljonny/'

export PATH=~/bin:~/.local/bin:$PATH

if [ -d "/opt/homebrew/bin" ]; then
  PATH="/opt/homebrew/bin:$PATH"
fi

if [ -d "$HOME/.bashrc.d/" ]; then
  while read -r BASH_FILE; do
    # shellcheck disable=SC1090
    source "$BASH_FILE"
  done < <(find "$HOME/.bashrc.d/" -mindepth 1 -maxdepth 1 -name '*.bashrc' | sort -n)
fi

function git-branch-finished {
  CURR_BRANCH=$(git branch --show-current)
  if git branch | grep -E '^\*?[^\S]+main$' >>/dev/null 2>&1; then
    git checkout main
  else
    git checkout master
  fi
  git pull -p
  git branch -d "$CURR_BRANCH"
}

if [ -f "$HOME/.asdf/asdf.sh" ]; then
  source "$HOME/.asdf/asdf.sh"
  source "$HOME/.asdf/completions/asdf.bash"
fi

function terraform-cleanup {
  find . -name '.terraform' -type d -print0 | xargs -0 rm -rf
}

function reboot-required-debian-based {
  if [ -f /var/run/reboot-required ]; then
    echo "Reboot IS required"
    return 1
  else
    echo "Reboot not required"
    return 0
  fi
}

function reboot-required {
  if command reboot-required >> /dev/null 2> /dev/null; then
    echo "COMMAND VERSION"
    command reboot-required
    return $?
  else
    if [ -f /etc/debian_version ]; then
      reboot-required-debian-based
      return $?
    else
      echo "Not a known linux variant. Cannot check if reboot is required"
      return 0
    fi
  fi
}

if command -v docker >> /dev/null 2>&1; then
  alias pandock='docker run --rm --volume "$(pwd):/data" --user $(id -u):$(id -g) pandoc/latex'
fi

. "$HOME/.cargo/env"
