#!/bin/bash

alias ej='eject /dev/sr0'
alias ej0='eject /dev/sr0'
alias ej1='eject /dev/sr1'
alias eja='eject /dev/sr0; eject /dev/sr1'
